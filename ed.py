import sympy as sp

x, y = sp.symbols("x y")
M = sp.Function("M")(x)
N = sp.Function("N")(y)

def firstMessage():
    print(chr(27)+"[1;34m"+"Exact Differential Equation - Calculator")
    print("\nInsert one equation in this form:")
    print(chr(27)+"[3;36m"+"\t\tM(x, y) dx + N(x, y) dy = 0")


def getExpresionsToString():
    firstMessage()
    stringExprM = input(chr(27)+"[0;32m"+"M(x, y) = ")
    stringExprN = input("N(x, y) = ")

    return [stringExprM, stringExprN]


def getExpresions():
    stringExpressions = getExpresionsToString()

    exprMsp = sp.sympify(stringExpressions[0])
    exprNsp = sp.sympify(stringExpressions[1])

    return [exprMsp, exprNsp]

def derivatesAreEquals(expressions):
    dMdy = sp.diff(expressions[0], y)
    dNdx = sp.diff(expressions[1], x)

    equals = False

    if dMdy == dNdx:
        equals = True
        print("\033[0;34m" + f'dM({expressions[0]})/dy = dN({expressions[1]})/dx')
        print(f'{dMdy} = {dNdx} \t-> '+'\033[0;33;40m'+'Exact Different Equation' + "\033[0;34m")
    else:
        print(f'dM({expressions[0]})/dy = dN({expressions[1]})/dx')
        print(f'{dMdy} != {dNdx} \t-> '+'\033[0;33;40m'+'No is an Exact Different Equation' + "\033[0;34m")
    return equals

def getIntegrates(expressions):
    M1 = sp.integrate(expressions[0], x)
    N1 = sp.integrate(expressions[1] - sp.diff(M1, y), y)

    print("M1 = Integrate(M(x, y))")
    print(f"M1 = Integrate({expressions[0]}) dx")
    print(f"M1 = {M1}\n")
    print("N1 = Integrate(N(xy) - d(M1)/dy) dy")
    print(f"N1 = Integrate({expressions[1]} - d({M1})/dy) dy")
    print(f"N1 = {N1}\n")
    return [M1, N1]

def sumIntegrates(integrates):
    expr = sp.simplify(integrates[0] + integrates[1])
    print("M1 + N1 = C1")
    print(f"{integrates[0]} + {integrates[1]} = C1")
    print(f"{expr} = C1")
    return expr

def solve():
    expressions = getExpresions()

    if derivatesAreEquals(expressions):
        print("\033[1;36m"+"\n\t\t |- SOLVING -| " + "\033[0;34m")
        integrates = getIntegrates(expressions)
        sumIntegrates(integrates)


if __name__ == '__main__':
    solve()
